﻿using System;

namespace Ssepan.Application.Mono
{
    public class CommandLineSwitch
    {
        #region Declarations
        #endregion Declarations

        #region Constructors
        //public CommandLineSwitch()
        //{ 
        //}

        public CommandLineSwitch
        (
            string switchCharacter, 
            string description, 
            bool usesValue, 
            Action<string, Action<string>> actionDelegate
        )
        {
            SwitchCharacter = switchCharacter;
            Description = description;
            UsesValue = usesValue;
            ActionDelegate = actionDelegate;
        }
        #endregion Constructors

        #region Properties
        private string _SwitchCharacter = default(string);
        public string SwitchCharacter
        {
            get { return _SwitchCharacter; }
            set { _SwitchCharacter = value; }
        }

        private string _Description = default(string);
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        private bool _UsesValue = default(bool);
        public bool UsesValue
        {
            get { return _UsesValue; }
            set { _UsesValue = value; }
        }

        private Action<string, Action<string>> _ActionDelegate = default(Action<string, Action<string>>);
        public Action<string, Action<string>> ActionDelegate
        {
            get { return _ActionDelegate; }
            set { _ActionDelegate = value; }
        }
        #endregion Properties

        #region Methods
        #endregion Methods
    }
}
