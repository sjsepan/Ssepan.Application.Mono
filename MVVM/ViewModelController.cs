﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Ssepan.Utility.Mono;

namespace Ssepan.Application.Mono
{
    /// <summary>
    /// Manager for the run-time model. 
    /// </summary>
    /// <typeparam name="TIcon"></typeparam>
    /// <typeparam name="TViewModel"></typeparam>
    public class ViewModelController<TIcon, TViewModel>
        where TViewModel :
            class,
            IViewModel<TIcon>,
            new()
        where TIcon : class
    {
        #region Declarations
        #endregion Declarations

        #region Constructors
        #endregion Constructors

        #region Properties
        private static Dictionary<string, TViewModel> _ViewModel = new Dictionary<string, TViewModel>();
        /// <summary>
        /// Allow for named viewmodels of a given type.
        /// </summary>
        public static Dictionary<string, TViewModel> ViewModel
        {
            get { return _ViewModel; }
            set { _ViewModel = value; }//TODO:notify here as well?
        }        
        #endregion Properties

        #region Methods
        /// <summary>
        /// add new viewmodel to dictionary
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static bool New
        (
            string viewName, 
            TViewModel viewModel
        )
        {
            bool returnValue = default(bool);
            try
            {
                //create new object
                ViewModel.Add(viewName, viewModel);

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
        /// <summary>
        /// add new viewmodel to dictionary
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static bool New_
        (
            string viewName,
            TViewModel viewModel
        )
        {
            bool returnValue = default(bool);
            try
            {
                returnValue = New(viewName, viewModel);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
        #endregion Methods
    }
}
