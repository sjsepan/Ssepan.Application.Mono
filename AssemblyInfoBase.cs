﻿using System;
using System.Reflection;

namespace Ssepan.Application.Mono
{

    #region " Helper class to get information for the About form. "
    // This class uses the System.Reflection.Assembly class to
    // access assembly meta-data
    // This class is ! a normal feature of AssemblyInfo.cs
    public class AssemblyInfoBase<TWindow>
    {
        // Used by Helper Functions to access information from Assembly Attributes
        protected Type myType;

        public AssemblyInfoBase()
        {
            myType = typeof(TWindow); 
        }

        public string AsmName
        {
            get
            {
                return myType.Assembly.GetName().Name.ToString();
            }
        }

        public string AsmFQName
        {
            get
            {
                return myType.Assembly.GetName().FullName.ToString();
            }
        }

        public string CodeBase
        {
            get
            {
                return myType.Assembly.Location;//myType.Assembly.CodeBase;
            }
        }

        public string Copyright
        {
            get
            {
                Type at = typeof(AssemblyCopyrightAttribute);
                object[] r = myType.Assembly.GetCustomAttributes(at, false);
                AssemblyCopyrightAttribute ct = (AssemblyCopyrightAttribute)r[0];
                return ct.Copyright;
            }
        }

        public string Company
        {
            get
            {
                Type at = typeof(AssemblyCompanyAttribute);
                object[] r = myType.Assembly.GetCustomAttributes(at, false);
                AssemblyCompanyAttribute ct = (AssemblyCompanyAttribute)r[0];
                return ct.Company;
            }
        }

        public string Description
        {
            get
            {
                Type at = typeof(AssemblyDescriptionAttribute);
                object[] r = myType.Assembly.GetCustomAttributes(at, false);
                AssemblyDescriptionAttribute da = (AssemblyDescriptionAttribute)r[0];
                return da.Description;
            }
        }

        public string Product
        {
            get
            {
                Type at = typeof(AssemblyProductAttribute);
                object[] r = myType.Assembly.GetCustomAttributes(at, false);
                AssemblyProductAttribute pt = (AssemblyProductAttribute)r[0];
                return pt.Product;
            }
        }

        public string Title
        {
            get
            {
                Type at = typeof(AssemblyTitleAttribute);
                object[] r = myType.Assembly.GetCustomAttributes(at, false);
                AssemblyTitleAttribute ta = (AssemblyTitleAttribute)r[0];
                return ta.Title;
            }
        }

        public string Version
        {
            get
            {
                return myType.Assembly.GetName().Version.ToString();
            }
        }
        
        private string _Website = null;
        public string Website
        {
            get
            {
                return _Website;
            }
            set
            {
                _Website = value;
            }
        }

        private string _WebsiteLabel = default(string);
        public string WebsiteLabel
        {
            get { return _WebsiteLabel; }
            set { _WebsiteLabel = value; }
        }

        private string[] _Designers = default(string[]);
        public string[] Designers
        {
            get { return _Designers; }
            set { _Designers = value; }
        }

        private string[] _Developers = default(string[]);
        public string[] Developers
        {
            get { return _Developers; }
            set { _Developers = value; }
        }

        private string[] _Documenters = default(string[]);
        public string[] Documenters
        {
            get { return _Documenters; }
            set { _Documenters = value; }
        }

        private string _License = default(string);
        public string License
        {
            get { return _License; }
            set { _License = value; }
        }

    }
    #endregion

}