﻿using System;

namespace Ssepan.Application.Mono
{
    public class DialogInfoBase<TWindow, TResponseEnum>
    {
#region Declarations
#endregion Declarations

#region Constructors
        public DialogInfoBase()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        // /// <param name="dialogFlags"></param>
        // /// <param name="buttonsType"></param>
        /// <param name="response"></param>
        /// <param name="errorMessage">Optional. Used when method cannot take ref params, such as async calls</param>
        public DialogInfoBase
        (
           TWindow parent,
            bool modal,
            string title,
            // TDialogFlags dialogFlags,
            // TButtonsType buttonsType,
            TResponseEnum response,
            string errorMessage = null
        )
        {
            Parent = parent;
            Modal = modal;
            Title = title;
            // DialogFlags = dialogFlags;
            // ButtonsType = buttonsType;
            Response = response;
            ErrorMessage = errorMessage;
        }
#endregion Constructors

#region Properties
        private TWindow _Parent = default(TWindow);
        public TWindow Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }

        private bool _Modal = default(bool);
        public bool Modal
        {
            get { return _Modal; }
            set { _Modal = value; }
        }
        private string _Title = default(string);
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        // private TDialogFlags _DialogFlags = default(TDialogFlags);
        // public TDialogFlags DialogFlags
        // {
        //     get { return _DialogFlags; }
        //     set { _DialogFlags = value; }
        // }

        // private TButtonsType _ButtonsType = default(TButtonsType);
        // public TButtonsType ButtonsType
        // {
        //     get { return _ButtonsType; }
        //     set { _ButtonsType = value; }
        // }

        private TResponseEnum _Response = default(TResponseEnum);
        public TResponseEnum Response
        {
            get { return _Response; }
            set { _Response = value; }
        }
        private string _ErrorMessage = default(string);
        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; }
        }

        private bool _BoolResult = default(bool);
        public bool BoolResult
        {
            get { return _BoolResult; }
            set { _BoolResult = value; }
        }

        private bool _Busy = default(bool);
        /// <summary>
        /// Semaphore used in some cases (Qml.Net) to indicate 
        /// dialog was started and has not completed yet.
        /// </summary>
        public bool Busy
        {
            get { return _Busy; }
            set { _Busy = value; }
        }

#endregion Properties

#region Methods
#endregion Methods

    }
}
