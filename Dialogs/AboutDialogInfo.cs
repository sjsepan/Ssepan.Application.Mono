﻿using System;
using System.Reflection;
using Ssepan.Utility.Mono;

namespace Ssepan.Application.Mono
{
    public class AboutDialogInfo<TWindow, TResponseEnum, TLogo> :
        DialogInfoBase<TWindow, TResponseEnum>
    {
#region Declarations
        public const string CopyrightSymbol = "©";
#endregion Declarations

#region Constructors
        public AboutDialogInfo() :
            base()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        /// <param name="response"></param>
        /// <param name="programName"></param>
        /// <param name="version"></param>
        /// <param name="copyright"></param>
        /// <param name="comments"></param>
        /// <param name="website"></param>
        /// <param name="logo"></param>
        public AboutDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            string programName,
            string version,
            string copyright,
            string comments,
            string website,
            TLogo logo
        ) :
            base (parent, modal, title, response)
        {
            try
            {
                ProgramName = programName;
                Version = version;
                Copyright = copyright;
                Comments = comments;
                Website = website;
                Logo = logo;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        /// <param name="response"></param>
        /// <param name="programName"></param>
        /// <param name="version"></param>
        /// <param name="copyright"></param>
        /// <param name="comments"></param>
        /// <param name="website"></param>
        /// <param name="logoFilename"></param>
        /// <param name="websiteLabel"></param>
        /// <param name="designers"></param>
        /// <param name="developers"></param>
        /// <param name="documenters"></param>
        /// <param name="license"></param>
        public AboutDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            string programName,
            string version,
            string copyright,
            string comments,
            string website,
            TLogo logo,
            string websiteLabel,
            string[] designers,
            string[] developers,
            string[] documenters,
            string license
        ) :
            this (parent, modal, title, response, programName, version, copyright, comments, website, logo)
        {
            try
            {
                WebsiteLabel = websiteLabel;
                Designers = designers;
                Developers = developers;
                Documenters = documenters;
                License = license;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

#endregion Constructors

#region Properties
        private string _ProgramName = default(string);
        public string ProgramName
        {
            get { return _ProgramName; }
            set { _ProgramName = value; }
        }

        private string _Version = default(string);
        public string Version
        {
            get { return _Version; }
            set { _Version = value; }
        }

        private string _Copyright = default(string);
        public string Copyright
        {
            get { return _Copyright; }
            set { _Copyright = value; }
        }

        private string _Comments = default(string);
        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        private string _Website = default(string);
        public string Website
        {
            get { return _Website; }
            set { _Website = value; }
        }

        private TLogo _Logo = default(TLogo);
        public TLogo Logo
        {
            get { return _Logo; }
            set { _Logo = value; }
        }

        private string _WebsiteLabel = default(string);
        public string WebsiteLabel
        {
            get { return _WebsiteLabel; }
            set { _WebsiteLabel = value; }
        }

        private string[] _Designers = default(string[]);
        public string[] Designers
        {
            get { return _Designers; }
            set { _Designers = value; }
        }

        private string[] _Developers = default(string[]);
        public string[] Developers
        {
            get { return _Developers; }
            set { _Developers = value; }
        }

        private string[] _Documenters = default(string[]);
        public string[] Documenters
        {
            get { return _Documenters; }
            set { _Documenters = value; }
        }

        private string _License = default(string);
        public string License
        {
            get { return _License; }
            set { _License = value; }
        }

#endregion Properties

#region Methods
#endregion Methods

    }
}
