﻿using System;

namespace Ssepan.Application.Mono
{
    public class ColorDialogInfo<TWindow, TResponseEnum, TColorDescriptor> :
        DialogInfoBase<TWindow, TResponseEnum>
    {
#region Declarations
#endregion Declarations

#region Constructors
        public ColorDialogInfo() :
            base()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        /// <param name="response"></param>
        /// <param name="color"></param>
        public ColorDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            TColorDescriptor color
        ) :
            base (parent, modal, title, response)
        {
            Color = color;
        }

#endregion Constructors

#region Properties
        private TColorDescriptor _Color = default(TColorDescriptor);
        public TColorDescriptor Color
        {
            get { return _Color; }
            set { _Color = value; }
        }
#endregion Properties

#region Methods
#endregion Methods

    }
}
