﻿using System;
// using System.Text;

namespace Ssepan.Application.Mono
{
    public class FileDialogInfo<TWindow, TResponseEnum> :
        DialogInfoBase<TWindow, TResponseEnum>
    {
        #region Declarations
        public const Char FILTER_SEPARATOR = ',';
        public const Char FILTER_ITEM_SEPARATOR = '|';
        public const string FILTER_FORMAT = "{0} (*.{1})|*.{1}";
        // public const string FILTER_DESCRIPTION = "{0} Files(s)";
        #endregion Declarations

        #region Constructors
        public FileDialogInfo() :
            base()
        { }

        /// <summary>
        /// Use this constructor if you want to use the SelectFolder FileChooserAction
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        /// <param name="response"></param>
        public FileDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response
        ) :
            base (parent, modal, title, response)
        {
        }

        /// <summary>
        /// Use this constructor if you want to default the location to an Environment.SpecialFolder.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        /// <param name="response"></param>
        /// <param name="newFilename"></param>
        /// <param name="filename"></param>
        /// <param name="extension"></param>
        /// <param name="description"></param>
        /// <param name="typeName"></param>
        /// <param name="additionalFilters"></param>
        /// <param name="multiselect"></param>
        /// <param name="initialDirectory"></param>
        /// <param name="forceDialog"></param>
        /// <param name="forceNew"></param>
        /// <param name="selectFolders"></param>
        public FileDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            string newFilename,
            string filename,
            string extension,
            string description,
            string typeName,
            string[] additionalFilters,
            bool multiselect,
            Environment.SpecialFolder initialDirectory,
            bool forceDialog,
            bool forceNew,
            bool selectFolders = false
        ) :
            base (parent, modal, title, response)
        {
            NewFilename = newFilename;
            Filename = filename;
            Extension = extension;
            Description = description;
            TypeName = typeName;
            //Join additionalFilters, create new array of those plus primary filter, then Join them again.
            Filters = string.Format(FILTER_FORMAT, description, extension);//TODO:do as described when we figure out why xbuild does't like Join()
            Multiselect = multiselect;
            InitialDirectory = initialDirectory;
            ForceDialog = forceDialog;
            ForceNew = forceNew;
            SelectFolders = selectFolders;
        }

        /// <summary>
        /// Use this constructor if you want to default the location to a custom location.
        /// Pass default(Environment.SpecialFolder) for InitialDirectory.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        /// <param name="response"></param>
        /// <param name="newFilename"></param>
        /// <param name="filename"></param>
        /// <param name="extension"></param>
        /// <param name="description"></param>
        /// <param name="typeName"></param>
        /// <param name="additionalFilters"></param>
        /// <param name="multiselect"></param>
        /// <param name="initialDirectory"></param>
        /// <param name="forceDialog"></param>
        /// <param name="forceNew"></param>
        /// <param name="customInitialDirectory"></param>
        /// <param name="selectFolders"></param>
        public FileDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            string newFilename,
            string filename,
            string extension,
            string description,
            string typeName,
            string[] additionalFilters,
            bool multiselect,
            Environment.SpecialFolder initialDirectory,
            bool forceDialog,
            bool forceNew,
            string customInitialDirectory,
            bool selectFolders = false
        ) : 
            this
            (
                parent,
                modal,
                title,
                response,
                newFilename,
                filename,
                extension,
                description,
                typeName,
                additionalFilters,
                multiselect,
                initialDirectory,
                forceDialog,
                forceNew,
                selectFolders
            )
        {
            CustomInitialDirectory = customInitialDirectory;
        }
        #endregion Constructors

        #region Properties
        private string _NewFilename = default(string);
        public string NewFilename
        {
            get { return _NewFilename; }
            set { _NewFilename = value; }
        }

        /// <summary>
        /// Default filename/path passed in; result Filename/path passed back out.
        private string _Filename = default(string);
        public string Filename
        {
            get { return _Filename; }
            set { _Filename = value; }
        }

        private string[] _Filenames = default(string[]);
        public string[] Filenames
        {
            get { return _Filenames; }
            set { _Filenames = value; }
        }

        private bool _MustExist = default(bool);
        public bool MustExist
        {
            get { return _MustExist; }
            set { _MustExist = value; }
        }

        private bool _Multiselect = default(bool);
        public bool Multiselect
        {
            get { return _Multiselect; }
            set { _Multiselect = value; }
        }

        private string _Extension = default(string);
        public string Extension
        {
            get { return _Extension; }
            set { _Extension = value; }
        }

        private string _Description = default(string);
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        private string _TypeName = default(string);
        public string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }

        private string _Filters = default(string);
        public string Filters
        {
            get { return _Filters; }
            set { _Filters = value; }
        }

        private Environment.SpecialFolder _InitialDirectory = default(Environment.SpecialFolder);
        public Environment.SpecialFolder InitialDirectory
        {
            get { return _InitialDirectory; }
            set { _InitialDirectory = value; }
        }

        private bool _ForceDialog = default(bool);
        public bool ForceDialog
        {
            get { return _ForceDialog; }
            set { _ForceDialog = value; }
        }

        private bool _ForceNew = default(bool);
        public bool ForceNew
        {
            get { return _ForceNew; }
            set { _ForceNew = value; }
        }

        private string _CustomInitialDirectory = default(string);
        public string CustomInitialDirectory
        {
            get { return _CustomInitialDirectory; }
            set { _CustomInitialDirectory = value; }
        }

        private bool _SelectFolders = default(bool);
        public bool SelectFolders
        {
            get { return _SelectFolders; }
            set { _SelectFolders = value; }
        }

        private bool _RestoreDirectory = default(bool);
        public bool RestoreDirectory
        {
            get { return _RestoreDirectory; }
            set { _RestoreDirectory = value; }
        }
        #endregion Properties

        #region Methods
        #endregion Methods

    }
}
