﻿using System;

namespace Ssepan.Application.Mono
{
    ///<summary>
    /// Note: added TColorDescriptor to support Mono WinForms FontDialog; just pass object for other platforms
    ///</summary>
    public class FontDialogInfo<TWindow, TResponseEnum, TFontDescriptor, TColorDescriptor> :
        DialogInfoBase<TWindow, TResponseEnum>
    {
        #region Declarations
        #endregion Declarations

        #region Constructors
        public FontDialogInfo() :
            base()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        /// <param name="response"></param>
        /// <param name="fontDescription"></param>
        public FontDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            TFontDescriptor fontDescription
        ) :
            base (parent, modal, title, response)
        {
            FontDescription = fontDescription;
        }

        ///<summary>
        /// Note: added TColorDescriptor to support Mono WinForms FontDialog; pass System.Drawing.Color, or just pass object for other platforms
        ///</summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        /// <param name="response"></param>
        /// <param name="fontDescription"></param>
        /// <param name="colorDescription"></param>
        public FontDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            TFontDescriptor fontDescription,
            TColorDescriptor colorDescription
        ) :
            this (parent, modal, title, response, fontDescription)
        {
            ColorDescription = colorDescription;
        }

        #endregion Constructors

        #region Properties
        private TFontDescriptor _FontDescription = default(TFontDescriptor);
        public TFontDescriptor FontDescription
        {
            get { return _FontDescription; }
            set { _FontDescription = value; }
        }
        private TColorDescriptor _ColorDescription = default(TColorDescriptor);
        public TColorDescriptor ColorDescription
        {
            get { return _ColorDescription; }
            set { _ColorDescription = value; }
        }
        #endregion Properties

        #region Methods
        #endregion Methods

    }
}
