﻿using System;

namespace Ssepan.Application.Mono
{
    public class PrinterDialogInfo<TWindow, TResponseEnum, TPrinter>  :
        DialogInfoBase<TWindow, TResponseEnum>
    {
#region Declarations
#endregion Declarations

#region Constructors
        public PrinterDialogInfo() :
            base()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        /// <param name="response"></param>
        public PrinterDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response
        ) :
            base (parent, modal, title, response)
        {
        }

#endregion Constructors//

#region Properties
        private TPrinter _Printer = default(TPrinter);
        public TPrinter Printer
        {
            get { return _Printer; }
            set { _Printer = value; }
        }

        private string _Name = default(string);
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
#endregion Properties

#region Methods
#endregion Methods

    }
}
