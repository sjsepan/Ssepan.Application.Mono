﻿using System;

namespace Ssepan.Application.Mono
{
    public class MessageDialogInfo<TWindow, TResponseEnum, TDialogFlags, TMessageType, TButtonsType> :
        DialogInfoBase<TWindow, TResponseEnum>
    {
#region Declarations
#endregion Declarations

#region Constructors
        public MessageDialogInfo() :
            base()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        /// <param name="dialogFlags"></param>
        /// <param name="messageType"></param>
        /// <param name="buttonsType"></param>
        /// <param name="message"></param>
        /// <param name="response"></param>
        public MessageDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TDialogFlags dialogFlags,
            TMessageType messageType,
            TButtonsType buttonsType,
            string message,
            TResponseEnum response
        ) :
            base (parent, modal, title, response)
        {
            DialogFlags = dialogFlags;
            MessageType = messageType;
            ButtonsType = buttonsType;
            Message = message;
            Response = response;
        }
#endregion Constructors

#region Properties

        private TDialogFlags _DialogFlags = default(TDialogFlags);
        public TDialogFlags DialogFlags
        {
            get { return _DialogFlags; }
            set { _DialogFlags = value; }
        }

        private TMessageType _MessageType = default(TMessageType);
        public TMessageType MessageType
        {
            get { return _MessageType; }
            set { _MessageType = value; }
        }

        private TButtonsType _ButtonsType = default(TButtonsType);
        public TButtonsType ButtonsType
        {
            get { return _ButtonsType; }
            set { _ButtonsType = value; }
        }

        private string _Message = default(string);
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        private string _Message2 = default(string);
        public string Message2
        {
            get { return _Message2; }
            set { _Message2 = value; }
        }

        private string _Message3 = default(string);
        public string Message3
        {
            get { return _Message3; }
            set { _Message3 = value; }
        }

#endregion Properties

#region Methods
#endregion Methods

    }
}
