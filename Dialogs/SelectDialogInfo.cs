﻿using System;

namespace Ssepan.Application.Mono
{
    public class SelectDialogInfo<TWindow, TResponseEnum, TLogo, TDataSource> :
        DialogInfoBase<TWindow, TResponseEnum>
    {
#region Declarations
#endregion Declarations

#region Constructors
        public SelectDialogInfo() :
            base()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="modal"></param>
        /// <param name="title"></param>
        /// <param name="response"></param>
        /// <param name="logo"></param>
        /// <param name="itemsName"></param>
        /// <param name="message"></param>
        /// <param name="itemsSource"></param>
        /// <param name="displayMember"></param>
        /// <param name="valueMember"></param>
        /// <param name="selectedIndex"></param>
        /// <param name="selectedText"></param>
        public SelectDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            TLogo logo,
            string itemsName,
            string message,
            TDataSource itemsSource, 
            string displayMember,
            string valueMember,
            object selectedItem,
            int selectedIndex,
            string selectedText
        ) :
            base (parent, modal, title, response)
        {
            Logo = logo;
            ItemsName = itemsName;
            Message = message;
            Response = response;
            ItemsSource = itemsSource;
            DisplayMember = displayMember;
            ValueMember = valueMember;
            SelectedItem = selectedItem;
            SelectedIndex = selectedIndex;
            SelectedText = selectedText;
        }
#endregion Constructors

#region Properties

        // private TDialogFlags _DialogFlags = default(TDialogFlags);
        // public TDialogFlags DialogFlags
        // {
        //     get { return _DialogFlags; }
        //     set { _DialogFlags = value; }
        // }

        // private TMessageType _MessageType = default(TMessageType);
        // public TMessageType MessageType
        // {
        //     get { return _MessageType; }
        //     set { _MessageType = value; }
        // }

        // private TButtonsType _ButtonsType = default(TButtonsType);
        // public TButtonsType ButtonsType
        // {
        //     get { return _ButtonsType; }
        //     set { _ButtonsType = value; }
        // }

        private TLogo _Logo = default(TLogo);
        public TLogo Logo
        {
            get { return _Logo; }
            set { _Logo = value; }
        }

        private string _ItemsName = default(string);
        public string ItemsName
        {
            get { return _ItemsName; }
            set { _ItemsName = value; }
        }

        private string _Message = default(string);
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        private TDataSource _ItemsSource = default(TDataSource);
        public TDataSource ItemsSource
        {
            get { return _ItemsSource; }
            set { _ItemsSource = value; }
        }

        private string _DisplayMember = default(string);
        public string DisplayMember
        {
            get { return _DisplayMember; }
            set { _DisplayMember = value; }
        }

        private string _ValueMember = default(string);
        public string ValueMember
        {
            get { return _ValueMember; }
            set { _ValueMember = value; }
        }

        private object _SelectedItem = default(object);
        public object SelectedItem
        {
            get { return _SelectedItem; }
            set { _SelectedItem = value; }
        }

        private int _SelectedIndex = default(int);
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set { _SelectedIndex = value; }
        }

        private string _SelectedText = default(string);
        public string SelectedText
        {
            get { return _SelectedText; }
            set { _SelectedText = value; }
        }

#endregion Properties

#region Methods
#endregion Methods

    }
}
