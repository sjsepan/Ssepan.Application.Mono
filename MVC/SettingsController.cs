using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml.Serialization;
// using System.Text.Json;
// using System.Text.Json.Serialization;
// using Newtonsoft.Json;
using Ssepan.Utility.Mono;
using Ssepan.Io.Mono;

namespace Ssepan.Application.Mono
{
    /// <summary>
    /// Manager for the persisted Settings. 
    /// </summary>
    /// <typeparam name="TSettings"></typeparam>
    public  class SettingsController<TSettings> //static
        where TSettings :
            class,
            ISettings,
            new()
    {
        #region Declarations
        public const string FILE_NEW = "(new)";
        #endregion Declarations

        #region Constructors
        #endregion Constructors

        #region Properties
        private static TSettings _Settings = default(TSettings);
        public static TSettings Settings//TODO:notify here as well?
        {
            get { return _Settings; }
            set 
            {
                if (DefaultHandler != null)
                {
                    if (_Settings != null)
                    {
                        _Settings.PropertyChanged -= DefaultHandler;
                    }
                }

                _Settings = value;

                if (DefaultHandler != null)
                {
                    if (_Settings != null)
                    {
                        _Settings.PropertyChanged += DefaultHandler;
                    }
                }
            }
        }

        private static string _OldPathname = default(string);
        /// <summary>
        /// Previous value of Pathname
        /// Set when filename changes.  Not synchronized here, but client apps may override and synchronize if it suits them.
        /// </summary>
        public static string OldPathname
        {
            get { return _OldPathname; }
            set
            {
                _OldPathname = value;
            }
        }

        private static string _OldFilename = default(string);
        /// <summary>
        /// Previous value of Filename.
        /// Set when filename changes.  Not synchronized here, but client apps may override and synchronize if it suits them.
        /// </summary>
        public static string OldFilename
        {
            get { return _OldFilename; }
            set
            {
                _OldFilename = value;
            }
        }

        private static string _Filename = FILE_NEW; 
        /// <summary>
        /// Filename component of FilePath
        /// </summary>
        public static string Filename
        {
            get { return _Filename; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    //just clear property
                    OldFilename = _Filename;//remember previous name
                    _Filename = string.Empty;
                }
                else if (Path.GetDirectoryName(value) != string.Empty)
                {
                    //send to be split first
                    FilePath = value;
                }
                else
                {
                    //just set property to value
                    OldFilename = _Filename;//remember previous name
                    _Filename = value;
                }
            }
        }

        private static string _Pathname = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).WithTrailingSeparator(); 
        /// <summary>
        /// Path component of FilePath
        /// </summary>
        public static string Pathname
        {
            get { return _Pathname; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    //just clear property
                    OldPathname = _Pathname;
                    _Pathname = value;
                }
                else if (Path.GetFileName(value) != string.Empty)
                {
                    //send to be split first
                    FilePath = value;
                }
                else
                {
                    //just set property to value
                    OldPathname = _Pathname;
                    _Pathname = value;
                }
            }
        }

        /// <summary>
        /// Combined value of Pathname and Filename
        /// </summary>
        public static string FilePath
        {
            get 
            { 
                //retrieve and combine
                return Path.Combine(Pathname, Filename); 
            }
            set
            {
                //split and store
                Filename = Path.GetFileName(value);
                if (!Path.GetDirectoryName(value).EndsWith(Path.DirectorySeparatorChar.ToString()))
                {
                    //add separator or Pathname setter will think path still contains filename
                    Pathname = Path.GetDirectoryName(value).WithTrailingSeparator();
                }
            }
        }
        
        private static PropertyChangedEventHandler _DefaultHandler = default(PropertyChangedEventHandler);
        /// <summary>
        /// Handler to assigned to Settings; triggered on New, Open.
        /// </summary>
        public static PropertyChangedEventHandler DefaultHandler
        {
            get { return _DefaultHandler; }
            set 
            {
                if (DefaultHandler != null)
                {
                    if (Settings != null)
                    {
                        Settings.PropertyChanged -= DefaultHandler;
                    }
                }

                _DefaultHandler = value;

                if (DefaultHandler != null)
                {
                    if (Settings != null)
                    {
                        Settings.PropertyChanged += DefaultHandler;
                    }
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// New settings
        /// </summary>
        /// <returns></returns>
        public static bool New()
        {
            bool returnValue = default(bool);
            try
            {//DEBUG:why is settings controller calling new before defaultHandler is set?
                //create new object
                Settings = new TSettings();
                Settings.Sync();
                Filename = FILE_NEW;

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }

        /// <summary>
        /// Open settings.
        /// </summary>
        /// <returns></returns>
        public static bool Open()
        {
            bool returnValue = default(bool);

            try
            {
                //read from file
                switch (SettingsBase.SerializeAs)
                {
                    case SettingsBase.SerializationFormat.Json:
                        {
                            Settings = LoadJson(FilePath);

                            break;
                        }
                    case SettingsBase.SerializationFormat.DataContract:
                        {
                            Settings = LoadDataContract(FilePath);

                            break;
                        }
                    case SettingsBase.SerializationFormat.Xml:
                    default:
                        {
                            Settings = LoadXml(FilePath);

                            break;
                        }
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        /// <summary>
        /// Save settings.
        /// </summary>
        /// <returns></returns>
        public static bool Save()
        {
            bool returnValue = default(bool);

            try
            {
                //write to file
                switch (SettingsBase.SerializeAs)
                {
                    case SettingsBase.SerializationFormat.Json:
                        {
                            PersistJson(Settings, FilePath);

                            break;
                        }
                    case SettingsBase.SerializationFormat.DataContract:
                        {
                            PersistDataContract(Settings, FilePath);

                            break;
                        }
                    case SettingsBase.SerializationFormat.Xml:
                    default:
                        {
                            PersistXml(Settings, FilePath);

                            break;
                        }
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
             
            return returnValue;
        }


        /// <summary>
        /// Loads the specified object with data from the specified file, using Json Serializer.
        /// </summary>
        /// <param name="filePath"></param>
        public static TSettings LoadJson(string filePath)
        {
            TSettings returnValue = default(TSettings);
            // Type returnValueType = default(Type);
            // JsonSerializerSettings jsonSerializerSettings = null;
            // JsonSerializer serializer = null;

            try
            {
                throw new NotImplementedException("LoadJson requires NewtonSoft.JSON");
                // returnValue = null;
                
                // jsonSerializerSettings = 
                // new JsonSerializerSettings
                // {
                //     MaxDepth = null
                // };

                // // deserialize JSON directly from a file
                // using (StreamReader file = File.OpenText(filePath))
                // {//TODO:use jsonSerializerSettings
                //     serializer = new JsonSerializer();
                //     returnValue = (TSettings)serializer.Deserialize(file, typeof(TSettings));
                //     //OR
                //     //TSettings account = JsonConvert.DeserializeObject<TSettings>(json);
                // }

                // // //Json Serializer of type Settings
                // // returnValueType = typeof(TSettings);//returnValue.GetType();
                // // jsonSerializerOptions = new JsonSerializerOptions();
                // // jsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve;
                // // jsonSerializerOptions.MaxDepth = int.MaxValue;
                // // jsonSerializerOptions.IgnoreReadOnlyProperties = true;

                // // //Stream writer for file
                // // using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                // // {
                // //     //de-serialize into Settings
                // //     returnValue = /*(TSettings)*/JsonSerializer.Deserialize<TSettings>(fs, jsonSerializerOptions);
                // //     // Console.WriteLine(string.Format("After opening {0} format is {1}...", filePath, SettingsBase.SerializeAs.ToString()));
                // // }

                // returnValue.Sync();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Saves the specified object's data to the specified file, using Json Serializer.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="filePath"></param>
        public static void PersistJson(TSettings settings, string filePath)
        {
            // JsonSerializerSettings jsonSerializerSettings = null;
            // JsonSerializer serializer = null;

            try
            {
                throw new NotImplementedException("PersistJson requires NewtonSoft.JSON");
                // jsonSerializerSettings = 
                // new JsonSerializerSettings
                // {
                //     MaxDepth = null
                // };

                // // serialize JSON directly to a file
                // using (StreamWriter file = File.CreateText(filePath))
                // {//TODO:use jsonSerializerSettings
                //     serializer = new JsonSerializer();
                //     serializer.Serialize(file, settings, typeof(TSettings));
                //     //OR
                //     //string json = JsonConvert.SerializeObject(settings, Formatting.Indented);
                // }                
                
                // // //Json Serializer of type Settings
                // // jsonSerializerOptions = new JsonSerializerOptions();
                // // jsonSerializerOptions.WriteIndented = true;
                // // jsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve;
                // // jsonSerializerOptions.MaxDepth = int.MaxValue;
                // // jsonSerializerOptions.IgnoreReadOnlyProperties = true;

                // // //Stream writer for file
                // // using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.Write))
                // // {
                // //     //serialize out of Settings
                // //     JsonSerializer.Serialize<TSettings>(fs, settings, jsonSerializerOptions);
                // // }

                // settings.Sync();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        /// <summary>
        /// Loads the specified object with data from the specified file, using DataContract Serializer.
        /// </summary>
        /// <param name="filePath"></param>
        public static TSettings LoadDataContract(string filePath)
        {
            TSettings returnValue = default(TSettings);
            Type returnValueType = default(Type);
            DataContractSerializerSettings dataContractSerializerSettings = null;

            try
            {
                //DataContract Serializer of type Settings
                returnValueType = typeof(TSettings);//returnValue.GetType();
                dataContractSerializerSettings = new DataContractSerializerSettings();
                dataContractSerializerSettings.PreserveObjectReferences=true;
                dataContractSerializerSettings.MaxItemsInObjectGraph = int.MaxValue;
                DataContractSerializer xs = new DataContractSerializer(returnValueType, dataContractSerializerSettings);//,null, int.MaxValue, false, true /* preserve object refs */, null

                //Stream writer for file
                using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    //de-serialize into Settings
                    returnValue = (TSettings)xs.ReadObject(fs);
                }

                returnValue.Sync();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Saves the specified object's data to the specified file, using DataContract Serializer.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="filePath"></param>
        public static void PersistDataContract(TSettings settings, string filePath)
        {
            DataContractSerializerSettings dataContractSerializerSettings = null;
            DataContractSerializer xs = null;

            try
            {
                //DataContract Serializer of type Settings
                dataContractSerializerSettings = new DataContractSerializerSettings();
                dataContractSerializerSettings.PreserveObjectReferences=true;
                dataContractSerializerSettings.MaxItemsInObjectGraph = int.MaxValue;
                xs = new DataContractSerializer(settings.GetType(), dataContractSerializerSettings);//, null, int.MaxValue, false, true /* preserve object refs */, null

                //Stream writer for file
                using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.Write))
                {
                    //serialize out of Settings
                    xs.WriteObject(fs, settings);
                }

                settings.Sync();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        /// <summary>
        /// Loads the specified object with data from the specified file, using XML Serializer.
        /// </summary>
        /// <param name="filePath"></param>
        public static TSettings LoadXml(string filePath)
        {
            TSettings returnValue = default(TSettings);
            Type returnValueType = default(Type);

            try
            {
                //XML Serializer of type Settings
                returnValueType = typeof(TSettings);//returnValue.GetType();
                XmlSerializer xs = new XmlSerializer(returnValueType);

                //Stream reader for file
                StreamReader sr = new StreamReader(filePath);

                //de-serialize into Settings
                returnValue = (TSettings)xs.Deserialize(sr);
                returnValue.Sync();

                //close file
                sr.Close();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Saves the specified object's data to the specified file, using XML Serializer.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="filePath"></param>
        public static void PersistXml(TSettings settings, string filePath)
        {
            try
            {
                //XML Serializer of type Settings
                XmlSerializer xs = new XmlSerializer(settings.GetType());

                //Stream writer for file
                StreamWriter sw = new StreamWriter(filePath);

                //serialize out of Settings
                xs.Serialize(sw, settings);

                //close file
                sw.Close();

                settings.Sync();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        /// <summary>
        /// Display property grid dialog. Changes to properties are reflected immediately.
        /// </summary>
        public static void ShowProperties(Action refreshDelegate)
        {
            try
            {
                throw new NotImplementedException("PropertiesViewer not available in GtkSharp");
//                 //PropertiesViewer pv = new PropertiesViewer(SettingsBase.AsStatic, Refresh);
//                 PropertyDialog pv = new PropertyDialog(Settings, refreshDelegate);
// #if debug
//                 //pv.Show();//dialog properties grid validation does refresh
// #else
//                 pv.ShowDialog();//dialog properties grid validation does refresh
//                 pv.Dispose();
// #endif
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);


                throw;
            }
        }
        #endregion Methods
    }
}
