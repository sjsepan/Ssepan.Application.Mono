# readme.md - README for Ssepan.Application.Mono

## About

Common library of application functions for C# Mono applications; requires ssepan.io.mono, ssepan.utility.mono

### Purpose

To encapsulate common application functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### Instructions for downloading/installing Newtonsoft.Json

JSON serialization is used by Ssepan.Application.Mono, but is not available in System.Text[.Json.Serialization]. To get JSON working, install from package manager:
<https://www.codeproject.com/Questions/747284/Json-NET-doesnt-work-with-Mono-under-Debian>
<https://stackoverflow.com/questions/36550910/what-is-the-json-net-mono-assembly-reference>
sudo apt-get install Newtonsoft.Json

### Issues

~Json read/write disabled for now
/usr/lib/mono/xbuild/14.0/bin/Microsoft.Common.targets:  warning : Reference 'System.Text' not resolved
/usr/lib/mono/xbuild/14.0/bin/Microsoft.Common.targets:  warning : Reference 'System.Text.Json' not resolved
/usr/lib/mono/xbuild/14.0/bin/Microsoft.Common.targets:  warning : Reference 'System.Text.Json.Serialization' not resolved
installed the Newtonsoft.Json package for ubuntu, but it does not handle the format correctly

### History

6.2:
~Bring console-handling into parity with .Net Core version of apps

6.1:
~Add SelectDialog

6.0:
~Initial release for Mono

Steve Sepan
<sjsepan@yahoo.com>
6/7/2024
